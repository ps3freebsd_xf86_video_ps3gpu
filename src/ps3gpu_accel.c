/*-
 * Copyright (C) 2011 glevand <geoffrey.levand@mail.ru>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer,
 *    without modification, immediately at the beginning of the file.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $FreeBSD$
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "xf86.h"
#include "xf86_OSproc.h"

#include "xf86Cursor.h"

#include "exa.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/uio.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>

#include "ps3gpu_ctl.h"

#include "ps3gpu.h"
#include "ps3gpu_accel.h"

enum {
	XFER_MODE_VIDEO_TO_VIDEO,
	XFER_MODE_GART_TO_VIDEO,
	XFER_MODE_VIDEO_TO_GART,
	XFER_MODE_GART_TO_GART
};

static inline void
Ps3GpuFifoPut(ScrnInfoPtr pScrn, CARD32 data)
{
	Ps3GpuPtr gPtr = PS3GPUPTR(pScrn);
	CARD32 *fifoBase = (CARD32 *) gPtr->fifoBase;

	fifoBase[gPtr->fifoCurrentSlot++] = data;
}

static inline void
Ps3GpuFifoKick(ScrnInfoPtr pScrn)
{
	Ps3GpuPtr gPtr = PS3GPUPTR(pScrn);
	volatile CARD32 *controlBase = (CARD32 *) gPtr->controlBase;

	if (gPtr->fifoCurrentSlot == gPtr->fifoPutSlot)
		return;

	controlBase[0x10] = gPtr->fifoGpuAddress + (gPtr->fifoCurrentSlot << 2);

	gPtr->fifoPutSlot = gPtr->fifoCurrentSlot;
}

static void
Ps3GpuFifoWait(ScrnInfoPtr pScrn, int slots)
{
	Ps3GpuPtr gPtr = PS3GPUPTR(pScrn);
	volatile CARD32 *controlBase = (CARD32 *) gPtr->controlBase;
	int retries = 5000;

	if ((gPtr->fifoFreeSlots - 1) >= slots)
		return;

	Ps3GpuFifoPut(pScrn, 0x20000000 | gPtr->fifoGpuAddress);

	controlBase[0x10] = gPtr->fifoGpuAddress;

	while (retries--) {
		if (controlBase[0x10] == controlBase[0x11])
			break;
	}

	if (controlBase[0x10] != controlBase[0x11])
		    FatalError("Failed to flush GPU FIFO\n");

	gPtr->fifoPutSlot = 0;
	gPtr->fifoCurrentSlot = 0;
	gPtr->fifoFreeSlots = gPtr->fifoMaxSlots - gPtr->fifoCurrentSlot;
}

static inline void
Ps3GpuFifoReserveSpace(ScrnInfoPtr pScrn, int slots)
{
	Ps3GpuPtr gPtr = PS3GPUPTR(pScrn);

	Ps3GpuFifoWait(pScrn, slots);

	gPtr->fifoFreeSlots -= slots;
}

static Bool
Ps3GpuContextInit(ScrnInfoPtr pScrn)
{
	Ps3GpuPtr gPtr = PS3GPUPTR(pScrn);

	Ps3GpuFifoReserveSpace(pScrn, 2);
	Ps3GpuFifoPut(pScrn, 0x00040000);
	Ps3GpuFifoPut(pScrn, 0x31337000);

	Ps3GpuFifoReserveSpace(pScrn, 2);
	Ps3GpuFifoPut(pScrn, 0x00042000);
	Ps3GpuFifoPut(pScrn, 0x31337303);

	Ps3GpuFifoReserveSpace(pScrn, 4);
	Ps3GpuFifoPut(pScrn, 0x000c2180);
	Ps3GpuFifoPut(pScrn, 0x66604200);
	Ps3GpuFifoPut(pScrn, 0xfeed0001);
	Ps3GpuFifoPut(pScrn, 0xfeed0000);

	Ps3GpuFifoReserveSpace(pScrn, 2);
	Ps3GpuFifoPut(pScrn, 0x00046000);
	Ps3GpuFifoPut(pScrn, 0x313371c3);

	Ps3GpuFifoReserveSpace(pScrn, 4);
	Ps3GpuFifoPut(pScrn, 0x000c6180);
	Ps3GpuFifoPut(pScrn, 0x66604200);
	Ps3GpuFifoPut(pScrn, 0xfeed0000);
	Ps3GpuFifoPut(pScrn, 0xfeed0000);

	Ps3GpuFifoReserveSpace(pScrn, 2);
	Ps3GpuFifoPut(pScrn, 0x0004a000);
	Ps3GpuFifoPut(pScrn, 0x31337808);

	Ps3GpuFifoReserveSpace(pScrn, 9);
	Ps3GpuFifoPut(pScrn, 0x0020a180);
	Ps3GpuFifoPut(pScrn, 0x66604200);
	Ps3GpuFifoPut(pScrn, 0x00000000);
	Ps3GpuFifoPut(pScrn, 0x00000000);
	Ps3GpuFifoPut(pScrn, 0x00000000);
	Ps3GpuFifoPut(pScrn, 0x00000000);
	Ps3GpuFifoPut(pScrn, 0x00000000);
	Ps3GpuFifoPut(pScrn, 0x00000000);
	Ps3GpuFifoPut(pScrn, 0x313371c3);

	Ps3GpuFifoReserveSpace(pScrn, 3);
	Ps3GpuFifoPut(pScrn, 0x0008a2fc);
	Ps3GpuFifoPut(pScrn, 0x00000003);
	Ps3GpuFifoPut(pScrn, 0x00000004);

	Ps3GpuFifoReserveSpace(pScrn, 2);
	Ps3GpuFifoPut(pScrn, 0x00048000);
	Ps3GpuFifoPut(pScrn, 0x31337a73);

	Ps3GpuFifoReserveSpace(pScrn, 3);
	Ps3GpuFifoPut(pScrn, 0x00088180);
	Ps3GpuFifoPut(pScrn, 0x66604200);
	Ps3GpuFifoPut(pScrn, 0xfeed0000);

	Ps3GpuFifoReserveSpace(pScrn, 2);
	Ps3GpuFifoPut(pScrn, 0x0004c000);
	Ps3GpuFifoPut(pScrn, 0x3137af00);

	Ps3GpuFifoReserveSpace(pScrn, 2);
	Ps3GpuFifoPut(pScrn, 0x0004c180);
	Ps3GpuFifoPut(pScrn, 0x66604200);

	Ps3GpuFifoKick(pScrn);

	return (TRUE);
}

static inline Bool
Ps3GpuTransferData(ScrnInfoPtr pScrn, int mode,
    CARD32 dst_offset, INT32 dst_pitch, CARD32 src_offset, INT32 src_pitch,
    CARD32 row_length, CARD32 row_count)
{
	Ps3GpuPtr gPtr = PS3GPUPTR(pScrn);
	CARD32 src, dst;
	CARD32 h;

	if (row_length == 0 || row_count == 0)
		return (TRUE);

	switch (mode) {
	case XFER_MODE_VIDEO_TO_VIDEO:
		src = 0xfeed0000;
		dst = 0xfeed0000;
	break;
	case XFER_MODE_GART_TO_VIDEO:
		src = 0xfeed0001;
		dst = 0xfeed0000;
	break;
	case XFER_MODE_VIDEO_TO_GART:
		src = 0xfeed0000;
		dst = 0xfeed0001;
	break;
	case XFER_MODE_GART_TO_GART:
		src = 0xfeed0001;
		dst = 0xfeed0001;
	break;
	default:
		return (FALSE);
	}

	Ps3GpuFifoReserveSpace(pScrn, 3);
	Ps3GpuFifoPut(pScrn, 0x00082184);
	Ps3GpuFifoPut(pScrn, src);
	Ps3GpuFifoPut(pScrn, dst);

	while (row_count) {
		h = row_count;
		if (h > 2047)
			h = 2047;

		Ps3GpuFifoReserveSpace(pScrn, 9);
		Ps3GpuFifoPut(pScrn, 0x0020230c);
		Ps3GpuFifoPut(pScrn, src_offset);
		Ps3GpuFifoPut(pScrn, dst_offset);
		Ps3GpuFifoPut(pScrn, src_pitch);
		Ps3GpuFifoPut(pScrn, dst_pitch);
		Ps3GpuFifoPut(pScrn, row_length);
		Ps3GpuFifoPut(pScrn, h);
		Ps3GpuFifoPut(pScrn, 0x00000101);
		Ps3GpuFifoPut(pScrn, 0x00000000);

		src_offset += h * src_pitch;
		dst_offset += h * dst_pitch;
		row_count -= h;
	}

	Ps3GpuFifoKick(pScrn);

	return (TRUE);
}

static Bool
Ps3GpuUploadInline(PixmapPtr pDst, int x, int y, int w, int h,
    char *src, int src_pitch)
{
	ScrnInfoPtr pScrn = xf86Screens[pDst->drawable.pScreen->myNum];
	Ps3GpuPtr gPtr = PS3GPUPTR(pScrn);
	int cpp = pDst->drawable.bitsPerPixel >> 3;
	int row_length = w * cpp;

	/*FIXME*/

	return (FALSE);
}

static Bool
Ps3GpuUploadDma(PixmapPtr pDst, int x, int y, int w, int h,
    char *src, int src_pitch)
{
	ScrnInfoPtr pScrn = xf86Screens[pDst->drawable.pScreen->myNum];
	Ps3GpuPtr gPtr = PS3GPUPTR(pScrn);
	int cpp = pDst->drawable.bitsPerPixel >> 3;
	int dst_pitch = exaGetPixmapPitch(pDst);
	int dst_offset = exaGetPixmapOffset(pDst) + y * dst_pitch + x * cpp;
	int row_length = w * cpp;
	int max_row_count = gPtr->gartSize / row_length;
	int row_count;
	char *dst;
	int i;

	while (h) {
		row_count = h;
		if (row_count > max_row_count)
			row_count = max_row_count;

		dst = (char *) gPtr->gartBase;

		if (src_pitch == row_length) {
			memcpy(dst, src, src_pitch * row_count);
			src += src_pitch * row_count;
		} else {
			for (i = 0; i < row_count; i++) {
				memcpy(dst, src, row_length);
				dst += row_length;
				src += src_pitch;
			}
		}

		Ps3GpuTransferData(pScrn, XFER_MODE_GART_TO_VIDEO,
		    gPtr->fbGpuAddress + dst_offset, dst_pitch,
		    gPtr->gartGpuAddress, row_length,
		    row_length, row_count);

		exaWaitSync(pDst->drawable.pScreen);

		dst_offset += row_count * dst_pitch;
		h -= row_count;
	}

	return (TRUE);
}

static Bool
Ps3GpuDownloadDma(PixmapPtr pSrc, int x, int y, int w, int h,
    char *dst, int dst_pitch)
{
	ScrnInfoPtr pScrn = xf86Screens[pSrc->drawable.pScreen->myNum];
	Ps3GpuPtr gPtr = PS3GPUPTR(pScrn);
	int cpp = pSrc->drawable.bitsPerPixel >> 3;
	int src_pitch = exaGetPixmapPitch(pSrc);
	int src_offset = exaGetPixmapOffset(pSrc) + y * src_pitch + x * cpp;
	int row_length = w * cpp;
	int max_row_count = gPtr->gartSize / row_length;
	int row_count;
	char *src;
	int i;

	while (h) {
		row_count = h;
		if (row_count > max_row_count)
			row_count = max_row_count;

		Ps3GpuTransferData(pScrn, XFER_MODE_VIDEO_TO_GART,
		    gPtr->gartGpuAddress, row_length,
		    gPtr->fbGpuAddress + src_offset, src_pitch,
		    row_length, row_count);

		exaWaitSync(pSrc->drawable.pScreen);

		src = (char *) gPtr->gartBase;

		if (dst_pitch == row_length) {
			memcpy(dst, src, dst_pitch * row_count);
			dst += dst_pitch * row_count;
		} else {
			for (i = 0; i < row_count; i++) {
				memcpy(dst, src, row_length);
				dst += dst_pitch;
				src += row_length;
			}
		}

		src_offset += row_count * src_pitch;
		h -= row_count;
	}

	return (TRUE);
}

static Bool
Ps3GpuPrepareSolid(PixmapPtr pPixmap, int alu, Pixel planemask, Pixel fg)
{
	/*FIXME*/

	return (FALSE);
}

static void
Ps3GpuSolid(PixmapPtr pPixmap, int x1, int y1, int x2, int y2)
{
	/*FIXME*/
}

static void
Ps3GpuDoneSolid(PixmapPtr pPixmap)
{
	/*FIXME*/
}

static Bool
Ps3GpuPrepareCopy(PixmapPtr pSrcPixmap, PixmapPtr pDstPixmap,
    int dx, int dy, int alu, Pixel planemask)
{
	ScrnInfoPtr pScrn = xf86Screens[pSrcPixmap->drawable.pScreen->myNum];
	Ps3GpuPtr gPtr = PS3GPUPTR(pScrn);

	DPRINTF(pScrn, "%s:%d: sbpp=%d dbpp=%d dx=%d dy=%d alu=%d\n",
	    __func__, __LINE__, pSrcPixmap->drawable.bitsPerPixel,
	    pDstPixmap->drawable.bitsPerPixel, dx, dy, alu);

	if (pSrcPixmap->drawable.bitsPerPixel !=
	    pDstPixmap->drawable.bitsPerPixel)
		return (FALSE);

	if (dx < 0 || dy < 0)
		return (FALSE);

	planemask |= ~0 << pDstPixmap->drawable.bitsPerPixel;

	if (planemask != ~0 || alu != GXcopy)
		return (FALSE);

	gPtr->copyDx = dx;
	gPtr->copyDy = dy;
	gPtr->pCopySrcPixmap = pSrcPixmap;

	return (TRUE);
}

static void
Ps3GpuCopy(PixmapPtr pDstPixmap, int srcX, int srcY, int dstX, int dstY,
    int width, int height)
{
	ScrnInfoPtr pScrn = xf86Screens[pDstPixmap->drawable.pScreen->myNum];
	Ps3GpuPtr gPtr = PS3GPUPTR(pScrn);
	int cpp = pDstPixmap->drawable.bitsPerPixel >> 3;
	int src_pitch = exaGetPixmapPitch(gPtr->pCopySrcPixmap);
	int src_offset = exaGetPixmapOffset(gPtr->pCopySrcPixmap) +
	    srcY * src_pitch + srcX * cpp;
	int dst_pitch = exaGetPixmapPitch(pDstPixmap);
	int dst_offset = exaGetPixmapOffset(pDstPixmap) +
	    dstY * dst_pitch + dstX * cpp;

	DPRINTF(pScrn, "%s:%d: srcX=%d srcY=%d dstX=%d dstY=%d width=%d height=%d\n",
	    __func__, __LINE__, srcX, srcY, dstX, dstY, width, height);

	Ps3GpuTransferData(pScrn, XFER_MODE_VIDEO_TO_VIDEO,
	    gPtr->fbGpuAddress + dst_offset, dst_pitch,
	    gPtr->fbGpuAddress + src_offset, src_pitch,
	    width * cpp, height);
}

static void
Ps3GpuDoneCopy(PixmapPtr pDstPixmap)
{
	ScrnInfoPtr pScrn = xf86Screens[pDstPixmap->drawable.pScreen->myNum];
	Ps3GpuPtr gPtr = PS3GPUPTR(pScrn);

	gPtr->pCopySrcPixmap = NULL;
}

static Bool
Ps3GpuUploadToScreen(PixmapPtr pDst, int x, int y, int w, int h,
    char *src, int src_pitch)
{
	ScrnInfoPtr pScrn = xf86Screens[pDst->drawable.pScreen->myNum];

#if 0
	if (Ps3GpuUploadInline(pDst, x, y, w, h, src, src_pitch))
		return (TRUE);

	if (Ps3GpuUploadDma(pDst, x, y, w, h, src, src_pitch))
		return (TRUE);
#endif

	return (FALSE);
}

static Bool
Ps3GpuDownloadFromScreen(PixmapPtr pSrc, int x, int y, int w, int h,
    char *dst, int dst_pitch)
{
	ScrnInfoPtr pScrn = xf86Screens[pSrc->drawable.pScreen->myNum];

#if 0
	if (Ps3GpuDownloadDma(pSrc, x, y, w, h, dst, dst_pitch))
		return (TRUE);
#endif

	return (FALSE);
}

static Bool
Ps3GpuCheckComposite(int op, PicturePtr pSrcPicture, PicturePtr pMaskPicture,
    PicturePtr pDstPicture) 
{
	/*FIXME*/

	return (FALSE);
}

static Bool
Ps3GpuPrepareComposite(int op, PicturePtr pSrcPicture,
    PicturePtr pMaskPicture, PicturePtr pDstPicture, PixmapPtr pSrc,
    PixmapPtr pMask, PixmapPtr pDst)
{
	/*FIXME*/

	return (FALSE);
}

static void
Ps3GpuComposite(PixmapPtr pDst, int srcX, int srcY, int maskX, int maskY,
    int dstX, int dstY, int width, int height)
{
	/*FIXME*/
}

static void
Ps3GpuDoneComposite(PixmapPtr pDst)
{
	/*FIXME*/
}

static int
Ps3GpuMarkSync(ScreenPtr screen)
{
	/*FIXME*/
}

static void
Ps3GpuWaitMarker(ScreenPtr pScreen, int marker)
{
	ScrnInfoPtr pScrn = xf86Screens[pScreen->myNum];
	Ps3GpuPtr gPtr = PS3GPUPTR(pScrn);
	volatile CARD32 *controlBase = (CARD32 *) gPtr->controlBase;
	int retries = 5000;

	Ps3GpuFifoKick(pScrn);

	while (retries--) {
		if (controlBase[0x10] == controlBase[0x11])
			break;
	}

	if (controlBase[0x10] != controlBase[0x11])
		    FatalError("Failed to flush GPU FIFO\n");
}

Bool
Ps3GpuAccelInit(ScreenPtr pScreen)
{
	ScrnInfoPtr pScrn = xf86Screens[pScreen->myNum];
	Ps3GpuPtr gPtr = PS3GPUPTR(pScrn);
	struct ps3gpu_ctl_memory_allocate memoryAllocate;
	struct ps3gpu_ctl_setup_control setupControl;
	ExaDriverPtr exaDriver;
	int err;

	gPtr->controlBase = (pointer) mmap(NULL, gPtr->gpuControlSize,
	    PROT_READ | PROT_WRITE, MAP_SHARED, gPtr->fd,
	    gPtr->gpuControlHandle);
	if (gPtr->controlBase == (pointer *) MAP_FAILED) {
	 	xf86DrvMsg(pScrn->scrnIndex, X_ERROR,
		    "Failed to map GPU control\n");
		return (FALSE);
	}

	gPtr->fifoSize = 64 * 1024;

	memoryAllocate.context_id = gPtr->gpuContextId;
	memoryAllocate.type = PS3GPU_CTL_MEMORY_TYPE_GART;
	memoryAllocate.size = gPtr->fifoSize;
	memoryAllocate.align = 12;

	err = ioctl(gPtr->fd, PS3GPU_CTL_MEMORY_ALLOCATE,
	    &memoryAllocate);
	if (err) {
		xf86DrvMsg(pScrn->scrnIndex, X_ERROR,
		    "Failed to allocate FIFO memory\n");
		return (FALSE);
	}

	gPtr->fifoHandle = memoryAllocate.handle;
	gPtr->fifoGpuAddress = memoryAllocate.gpu_addr;

	gPtr->fifoBase = (pointer) mmap(NULL, gPtr->fifoSize,
	    PROT_READ | PROT_WRITE, MAP_SHARED, gPtr->fd,
	    gPtr->fifoHandle);
	if (gPtr->fifoBase == (pointer *) MAP_FAILED) {
		xf86DrvMsg(pScrn->scrnIndex, X_ERROR,
		    "Failed to map FIFO memory\n");
		return (FALSE);
	}

	setupControl.context_id = gPtr->gpuContextId;
	setupControl.put = gPtr->fifoHandle;
	setupControl.get = gPtr->fifoHandle;
	setupControl.ref = 0xffffffff;

	err = ioctl(gPtr->fd, PS3GPU_CTL_SETUP_CONTROL, &setupControl);
	if (err < 0) {
		xf86DrvMsg(pScrn->scrnIndex, X_ERROR,
		    "Failed to setup GPU control\n");
		return (FALSE);
	}

	gPtr->fifoMaxSlots = (gPtr->fifoSize - 1032) >> 2;
	gPtr->fifoPutSlot = 0;
	gPtr->fifoCurrentSlot = 0;
	gPtr->fifoFreeSlots = gPtr->fifoMaxSlots - gPtr->fifoCurrentSlot;

	gPtr->gartSize = 1 * 1024 * 1024;

	memoryAllocate.context_id = gPtr->gpuContextId;
	memoryAllocate.type = PS3GPU_CTL_MEMORY_TYPE_GART;
	memoryAllocate.size = gPtr->gartSize;
	memoryAllocate.align = 12;

	err = ioctl(gPtr->fd, PS3GPU_CTL_MEMORY_ALLOCATE,
	    &memoryAllocate);
	if (err) {
		xf86DrvMsg(pScrn->scrnIndex, X_ERROR,
		    "Failed to allocate GART memory\n");
		return (FALSE);
	}

	gPtr->gartHandle = memoryAllocate.handle;
	gPtr->gartGpuAddress = memoryAllocate.gpu_addr;

	gPtr->gartBase = (pointer) mmap(NULL, gPtr->gartSize,
	    PROT_READ | PROT_WRITE, MAP_SHARED, gPtr->fd,
	    gPtr->gartHandle);
	if (gPtr->gartBase == (pointer *) MAP_FAILED) {
		xf86DrvMsg(pScrn->scrnIndex, X_ERROR,
		    "Failed to map GART memory\n");
		return (FALSE);
	}

	if (!Ps3GpuContextInit(pScrn))
		return (FALSE);

	exaDriver = exaDriverAlloc();
	if(!exaDriver)
		return (FALSE);

	gPtr->exaDriver = exaDriver;

	exaDriver->exa_major = EXA_VERSION_MAJOR;
	exaDriver->exa_minor = EXA_VERSION_MINOR;

	exaDriver->memoryBase = (CARD8 *) gPtr->fbBase;
	exaDriver->offScreenBase = pScrn->displayWidth * pScrn->virtualY *
	    (pScrn->bitsPerPixel >> 3);
	exaDriver->offScreenBase = (exaDriver->offScreenBase + 0x3f) & ~0x3f;
	exaDriver->memorySize = gPtr->fbSize;

	exaDriver->pixmapOffsetAlign = 256;
	exaDriver->pixmapPitchAlign = 64;
	exaDriver->maxX = 2048;
	exaDriver->maxY = 2048;

	exaDriver->flags = EXA_OFFSCREEN_PIXMAPS;

	exaDriver->PrepareSolid = Ps3GpuPrepareSolid;
	exaDriver->Solid = Ps3GpuSolid;
	exaDriver->DoneSolid = Ps3GpuDoneSolid;
	exaDriver->PrepareCopy = Ps3GpuPrepareCopy;
	exaDriver->Copy = Ps3GpuCopy;
	exaDriver->DoneCopy = Ps3GpuDoneCopy;
	exaDriver->UploadToScreen = Ps3GpuUploadToScreen;
	exaDriver->DownloadFromScreen = Ps3GpuDownloadFromScreen;
	exaDriver->CheckComposite = Ps3GpuCheckComposite;
	exaDriver->PrepareComposite = Ps3GpuPrepareComposite;
	exaDriver->Composite = Ps3GpuComposite;
	exaDriver->DoneComposite = Ps3GpuDoneComposite;
	exaDriver->MarkSync = Ps3GpuMarkSync;
	exaDriver->WaitMarker = Ps3GpuWaitMarker;

	gPtr->exaDriver = exaDriver;

	return (exaDriverInit(pScreen, exaDriver));
}
