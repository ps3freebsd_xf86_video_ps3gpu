/*-
 * Copyright (C) 2011 glevand <geoffrey.levand@mail.ru>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer,
 *    without modification, immediately at the beginning of the file.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $FreeBSD$
 */

#ifndef _PS3GPU_H
#define _PS3GPU_H

#ifdef DEBUG
#define DPRINTF(pScrn, ...)	xf86DrvMsg((pScrn)->scrnIndex, X_INFO, ##__VA_ARGS__)
#else
#define DPRINTF(pScrn, ...)
#endif

typedef struct {
	EntityInfoPtr pEnt;
	OptionInfoPtr options;
	CloseScreenProcPtr closeScreen;

	Bool shadowFB;
	Bool swCursor;
	Bool noAccel;

	int fd;
	int gpuContextId;
	unsigned long gpuControlHandle;
	int gpuControlSize;

	unsigned long fbHandle;
	unsigned int fbGpuAddress;
	pointer* fbBase;
	int fbSize;

	unsigned long cursorHandle;
	pointer* cursorBase;
	int cursorSize;
	Bool cursorShow;
	int cursorWidth;
	int cursorHeight;
	CARD32 cursorBgColor;
	CARD32 cursorFgColor;
	xf86CursorInfoPtr cursorInfo;

	pointer *controlBase;
	unsigned long fifoHandle;
	unsigned int fifoGpuAddress;
	pointer *fifoBase;
	int fifoSize;
	int fifoMaxSlots;
	int fifoPutSlot;
	int fifoCurrentSlot;
	int fifoFreeSlots;
	unsigned long gartHandle;
	unsigned int gartGpuAddress;
	pointer *gartBase;
	int gartSize;
	int copyDx;
	int copyDy;
	PixmapPtr pCopySrcPixmap;
	ExaDriverPtr exaDriver;
} Ps3GpuRec, *Ps3GpuPtr;

#define PS3GPUPTR(p)	((Ps3GpuPtr) (p)->driverPrivate)

#endif /* _PS3GPU_H */
