/*-
 * Copyright (C) 2011 glevand <geoffrey.levand@mail.ru>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer,
 *    without modification, immediately at the beginning of the file.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $FreeBSD$
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "xf86.h"
#include "xf86_OSproc.h"

#include "xf86Cursor.h"
#ifdef ARGB_CURSOR
#include "cursorstr.h"
#endif

#include "exa.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/uio.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>

#include "ps3gpu_ctl.h"

#include "ps3gpu.h"
#include "ps3gpu_cursor.h"

#define PS3GPU_CURSOR_WIDTH	64
#define PS3GPU_CURSOR_HEIGHT	64

static Bool
Ps3GpuCursorEnable(ScrnInfoPtr pScrn, Bool enable)
{
	Ps3GpuPtr gPtr = PS3GPUPTR(pScrn);
	struct ps3gpu_ctl_cursor_enable cursorEnable;
	int err;

	cursorEnable.context_id = gPtr->gpuContextId;
	cursorEnable.head = PS3GPU_CTL_HEAD_A;
	cursorEnable.enable = enable ? 1 : 0;

	err = ioctl(gPtr->fd, PS3GPU_CTL_CURSOR_ENABLE,
	    &cursorEnable);
	if (err) {
		xf86DrvMsg(pScrn->scrnIndex, X_ERROR,
		    "Failed to %s cursor for head A\n",
		    enable ? "enable" : "disable");
		return (FALSE);
	}

	cursorEnable.context_id = gPtr->gpuContextId;
	cursorEnable.head = PS3GPU_CTL_HEAD_B;
	cursorEnable.enable = enable ? 1 : 0;

	err = ioctl(gPtr->fd, PS3GPU_CTL_CURSOR_ENABLE,
	    &cursorEnable);
	if (err) {
		xf86DrvMsg(pScrn->scrnIndex, X_ERROR,
		    "Failed to %s cursor for head B\n",
		    enable ? "enable" : "disable");
		return (FALSE);
	}

	return (TRUE);
}

static Bool
Ps3GpuUseHWCursor(ScreenPtr pScreen, CursorPtr pCursor)
{
	ScrnInfoPtr pScrn = xf86Screens[pScreen->myNum];
	Ps3GpuPtr gPtr = PS3GPUPTR(pScrn);

	if (pCursor->bits->width > gPtr->cursorInfo->MaxWidth ||
	    pCursor->bits->height > gPtr->cursorInfo->MaxHeight)
		return (FALSE);

	gPtr->cursorWidth = pCursor->bits->width;
	gPtr->cursorHeight = pCursor->bits->height;

	return (TRUE);
}

static void
Ps3GpuSetCursorColors(ScrnInfoPtr pScrn, int bg, int fg)
{
	Ps3GpuPtr gPtr = PS3GPUPTR(pScrn);

	gPtr->cursorBgColor = 0xff000000 | bg;
	gPtr->cursorFgColor = 0xff000000 | fg;
}

static void
Ps3GpuLoadCursorImage(ScrnInfoPtr pScrn, unsigned char *src)
{
#define BIT(p, x)	((p)[(x) >> 5] & (1 << (31 - ((x) & 0x1f))))

	Ps3GpuPtr gPtr = PS3GPUPTR(pScrn);
	int size = gPtr->cursorInfo->MaxWidth * gPtr->cursorInfo->MaxHeight;
	CARD32 *pSrc = (CARD32 *) src;
	CARD32 *pMask = pSrc + (size >> 5);
	CARD32 *pDst = (CARD32 *) gPtr->cursorBase;
	int x, y;

	if (gPtr->cursorShow) {
		if (!Ps3GpuCursorEnable(pScrn, FALSE))
			return;
	}

	memset(pDst, 0, size * sizeof(CARD32));

	for (y = 0; y < gPtr->cursorHeight; y++) {
		for (x = 0; x < gPtr->cursorWidth; x++) {
			if (BIT(pMask, x)) {
				if (BIT(pSrc, x))
					pDst[x] = gPtr->cursorFgColor;
				else
					pDst[x] = gPtr->cursorBgColor;
			}
		}

		pSrc += gPtr->cursorInfo->MaxWidth >> 5;
		pMask += gPtr->cursorInfo->MaxWidth >> 5;
		pDst += gPtr->cursorInfo->MaxWidth;
	}

	if (gPtr->cursorShow) {
		if (!Ps3GpuCursorEnable(pScrn, TRUE))
			return;
	}

#undef BIT
}

static void
Ps3GpuSetCursorPosition(ScrnInfoPtr pScrn, int x, int y)
{
	Ps3GpuPtr gPtr = PS3GPUPTR(pScrn);
	struct ps3gpu_ctl_cursor_set_position cursorSetPosition;
	int err;

	if (gPtr->cursorShow) {
		if (!Ps3GpuCursorEnable(pScrn, FALSE))
			return;
	}

	cursorSetPosition.context_id = gPtr->gpuContextId;
	cursorSetPosition.head = PS3GPU_CTL_HEAD_A;
	cursorSetPosition.x = x;
	cursorSetPosition.y = y;

	err = ioctl(gPtr->fd, PS3GPU_CTL_CURSOR_SET_POSITION,
	    &cursorSetPosition);
	if (err) {
		xf86DrvMsg(pScrn->scrnIndex, X_ERROR,
		    "Failed to set cursor position for head A\n");
	}

	cursorSetPosition.context_id = gPtr->gpuContextId;
	cursorSetPosition.head = PS3GPU_CTL_HEAD_B;
	cursorSetPosition.x = x;
	cursorSetPosition.y = y;

	err = ioctl(gPtr->fd, PS3GPU_CTL_CURSOR_SET_POSITION,
	    &cursorSetPosition);
	if (err) {
		xf86DrvMsg(pScrn->scrnIndex, X_ERROR,
		    "Failed to set cursor position for head B\n");
	}

	if (gPtr->cursorShow) {
		if (!Ps3GpuCursorEnable(pScrn, TRUE))
			return;
	}
}

static void
Ps3GpuShowCursor(ScrnInfoPtr pScrn)
{
	Ps3GpuPtr gPtr = PS3GPUPTR(pScrn);

	if (!Ps3GpuCursorEnable(pScrn, TRUE))
		return;

	gPtr->cursorShow = TRUE;
}

static void
Ps3GpuHideCursor(ScrnInfoPtr pScrn)
{
	Ps3GpuPtr gPtr = PS3GPUPTR(pScrn);

	if (!Ps3GpuCursorEnable(pScrn, FALSE))
		return;

	gPtr->cursorShow = FALSE;
}

#ifdef ARGB_CURSOR

static Bool
Ps3GpuUseHWCursorARGB(ScreenPtr pScreen, CursorPtr pCursor)
{
	ScrnInfoPtr pScrn = xf86Screens[pScreen->myNum];
	Ps3GpuPtr gPtr = PS3GPUPTR(pScrn);

	if (pCursor->bits->width > gPtr->cursorInfo->MaxWidth ||
	    pCursor->bits->height > gPtr->cursorInfo->MaxHeight)
		return (FALSE);

	gPtr->cursorWidth = pCursor->bits->width;
	gPtr->cursorHeight = pCursor->bits->height;

	return (TRUE);
}

static void
Ps3GpuLoadCursorARGB(ScrnInfoPtr pScrn, CursorPtr pCursor)
{
	Ps3GpuPtr gPtr = PS3GPUPTR(pScrn);
	int size = gPtr->cursorInfo->MaxWidth * gPtr->cursorInfo->MaxHeight;
	CARD32 *pDst = (CARD32 *) gPtr->cursorBase;
	CARD32 *pSrc = pCursor->bits->argb;
	int x, y;

	memset(pDst, 0, size * sizeof(CARD32));

	for (y = 0; y < pCursor->bits->height; y++) {
		for (x = 0; x < pCursor->bits->width; x++)
			*pDst++ = *pSrc++;
	}
}

#endif /* ARGB_CURSOR */

Bool
Ps3GpuCursorInit(ScreenPtr pScreen)
{
	ScrnInfoPtr pScrn = xf86Screens[pScreen->myNum];
	Ps3GpuPtr gPtr = PS3GPUPTR(pScrn);
	xf86CursorInfoPtr cursorInfo;
	struct ps3gpu_ctl_memory_allocate memoryAllocate;
	struct ps3gpu_ctl_cursor_initialize cursorInitialize;
	struct ps3gpu_ctl_cursor_set_image cursorSetImage;
	int err;

	gPtr->cursorSize = (PS3GPU_CURSOR_WIDTH *
	    PS3GPU_CURSOR_HEIGHT * sizeof(CARD32) +
	    ((1 << 12) - 1)) & ~((1 << 12) - 1);

	memoryAllocate.context_id = gPtr->gpuContextId;
	memoryAllocate.type = PS3GPU_CTL_MEMORY_TYPE_VIDEO;
	memoryAllocate.size = gPtr->cursorSize;
	memoryAllocate.align = 20;

	err = ioctl(gPtr->fd, PS3GPU_CTL_MEMORY_ALLOCATE,
	    &memoryAllocate);
	if (err) {
		xf86DrvMsg(pScrn->scrnIndex, X_ERROR,
		    "Failed to allocate cursor memory\n");
		return (FALSE);
	}

	gPtr->cursorHandle = memoryAllocate.handle;

	gPtr->cursorBase = (pointer) mmap(NULL, gPtr->cursorSize,
	    PROT_READ | PROT_WRITE, MAP_SHARED, gPtr->fd,
	    gPtr->cursorHandle);
	if (gPtr->cursorBase == (pointer *) MAP_FAILED) {
		xf86DrvMsg(pScrn->scrnIndex, X_ERROR,
		    "Failed to map cursor memory\n");
		return (FALSE);
	}

	cursorInitialize.context_id = gPtr->gpuContextId;
	cursorInitialize.head = PS3GPU_CTL_HEAD_A;

	err = ioctl(gPtr->fd, PS3GPU_CTL_CURSOR_INITIALIZE,
	    &cursorInitialize);
	if (err) {
		f86DrvMsg(pScrn->scrnIndex, X_ERROR,
		    "Failed to initialize cursor for head A\n");
		return (FALSE);
	}

	cursorInitialize.context_id = gPtr->gpuContextId;
	cursorInitialize.head = PS3GPU_CTL_HEAD_B;

	err = ioctl(gPtr->fd, PS3GPU_CTL_CURSOR_INITIALIZE,
	    &cursorInitialize);
	if (err) {
		xf86DrvMsg(pScrn->scrnIndex, X_ERROR,
		    "Failed to initialize cursor for head B\n");
		return (FALSE);
	}

	if (!Ps3GpuCursorEnable(pScrn, FALSE))
		return (FALSE);

	cursorSetImage.context_id = gPtr->gpuContextId;
	cursorSetImage.head = PS3GPU_CTL_HEAD_A;
	cursorSetImage.offset = gPtr->cursorHandle;

	err = ioctl(gPtr->fd, PS3GPU_CTL_CURSOR_SET_IMAGE,
	    &cursorSetImage);
	if (err) {
		xf86DrvMsg(pScrn->scrnIndex, X_ERROR,
		    "Failed to set cursor image for head A\n");
		return (FALSE);
	}

	cursorSetImage.context_id = gPtr->gpuContextId;
	cursorSetImage.head = PS3GPU_CTL_HEAD_B;
	cursorSetImage.offset = gPtr->cursorHandle;

	err = ioctl(gPtr->fd, PS3GPU_CTL_CURSOR_SET_IMAGE,
	    &cursorSetImage);
	if (err) {
		xf86DrvMsg(pScrn->scrnIndex, X_ERROR,
		    "Failed to set cursor image for head B\n");
		return (FALSE);
	}

	cursorInfo = xf86CreateCursorInfoRec();
	if(!cursorInfo)
		return (FALSE);

	gPtr->cursorInfo = cursorInfo;

	cursorInfo->MaxWidth = PS3GPU_CURSOR_WIDTH;
	cursorInfo->MaxHeight = PS3GPU_CURSOR_HEIGHT;
	cursorInfo->Flags = HARDWARE_CURSOR_TRUECOLOR_AT_8BPP |
	    HARDWARE_CURSOR_SOURCE_MASK_NOT_INTERLEAVED;

	cursorInfo->UseHWCursor = Ps3GpuUseHWCursor;
	cursorInfo->SetCursorColors = Ps3GpuSetCursorColors;
	cursorInfo->SetCursorPosition = Ps3GpuSetCursorPosition;
	cursorInfo->LoadCursorImage = Ps3GpuLoadCursorImage;
	cursorInfo->HideCursor = Ps3GpuHideCursor;
	cursorInfo->ShowCursor = Ps3GpuShowCursor;

#ifdef ARGB_CURSOR

	cursorInfo->Flags |= HARDWARE_CURSOR_ARGB;

	cursorInfo->UseHWCursorARGB = Ps3GpuUseHWCursorARGB;
	cursorInfo->LoadCursorARGB = Ps3GpuLoadCursorARGB;

#endif

	return (xf86InitCursor(pScreen, cursorInfo));
}
