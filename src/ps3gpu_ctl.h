/*-
 * Copyright (C) 2011 glevand <geoffrey.levand@mail.ru>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer,
 *    without modification, immediately at the beginning of the file.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $FreeBSD$
 */

#ifndef _PS3GPU_CTL_H
#define _PS3GPU_CTL_H

struct ps3gpu_ctl_context_allocate {
	/* in */
	int vram_size;
	/* out */
	int context_id;
	unsigned long control_handle;
	int control_size;
	unsigned long driver_info_handle;
	int driver_info_size;
	unsigned long reports_handle;
	int reports_size;
};

struct ps3gpu_ctl_context_free {
	/* in */
	int context_id;
};

enum ps3gpu_ctl_memory_type {
	PS3GPU_CTL_MEMORY_TYPE_VIDEO,
	PS3GPU_CTL_MEMORY_TYPE_GART
};

struct ps3gpu_ctl_memory_allocate {
	/* in */
	int context_id;
	int type;
	int size;
	int align;
	/* out */
	unsigned long handle;
	unsigned int gpu_addr;
};

struct ps3gpu_ctl_memory_free {
	/* in */
	int context_id;
	unsigned long handle;
};

struct ps3gpu_ctl_setup_control {
	/* in */
	int context_id;
	unsigned long put;
	unsigned long get;
	unsigned int ref;
};

enum ps3gpu_ctl_head {
	PS3GPU_CTL_HEAD_A,
	PS3GPU_CTL_HEAD_B
};

enum ps3gpu_ctl_flip_mode {
	PS3GPU_CTL_FLIP_MODE_HSYNC,
	PS3GPU_CTL_FLIP_MODE_VSYNC
};

struct ps3gpu_ctl_set_flip_mode {
	/* in */
	int context_id;
	int head;
	int mode;
};

struct ps3gpu_ctl_reset_flip_status {
	/* in */
	int context_id;
	int head;
};

struct ps3gpu_ctl_flip {
	/* in */
	int context_id;
	int head;
	unsigned long offset;
};

struct ps3gpu_ctl_display_buffer_set {
	/* in */
	int context_id;
	int buffer_id;
	int width;
	int height;
	int pitch;
	unsigned long offset;
};

struct ps3gpu_ctl_display_buffer_unset {
	/* in */
	int context_id;
	int buffer_id;
};

struct ps3gpu_ctl_display_buffer_flip {
	/* in */
	int context_id;
	int head;
	int buffer_id;
};

struct ps3gpu_ctl_cursor_initialize {
	/* in */
	int context_id;
	int head;
};

struct ps3gpu_ctl_cursor_set_image {
	/* in */
	int context_id;
	int head;
	unsigned long offset;
};

struct ps3gpu_ctl_cursor_set_position {
	/* in */
	int context_id;
	int head;
	int x;
	int y;
};

struct ps3gpu_ctl_cursor_enable {
	/* in */
	int context_id;
	int head;
	int enable;
};

#define	PS3GPU_CTL_CONTEXT_ALLOCATE	_IOWR('G', 0, struct ps3gpu_ctl_context_allocate)
#define	PS3GPU_CTL_CONTEXT_FREE		_IOW('G', 1, struct ps3gpu_ctl_context_free)
#define	PS3GPU_CTL_MEMORY_ALLOCATE	_IOWR('G', 2, struct ps3gpu_ctl_memory_allocate)
#define	PS3GPU_CTL_MEMORY_FREE		_IOW('G', 3, struct ps3gpu_ctl_memory_free)
#define	PS3GPU_CTL_SETUP_CONTROL	_IOW('G', 4, struct ps3gpu_ctl_setup_control)
#define	PS3GPU_CTL_SET_FLIP_MODE	_IOW('G', 5, struct ps3gpu_ctl_set_flip_mode)
#define	PS3GPU_CTL_RESET_FLIP_STATUS	_IOW('G', 6, struct ps3gpu_ctl_reset_flip_status)
#define	PS3GPU_CTL_FLIP			_IOW('G', 7, struct ps3gpu_ctl_flip)
#define	PS3GPU_CTL_DISPLAY_BUFFER_SET	_IOW('G', 8, struct ps3gpu_ctl_display_buffer_set)
#define	PS3GPU_CTL_DISPLAY_BUFFER_UNSET	_IOW('G', 9, struct ps3gpu_ctl_display_buffer_unset)
#define	PS3GPU_CTL_DISPLAY_BUFFER_FLIP	_IOW('G', 10, struct ps3gpu_ctl_display_buffer_flip)
#define	PS3GPU_CTL_CURSOR_INITIALIZE	_IOW('G', 11, struct ps3gpu_ctl_cursor_initialize)
#define	PS3GPU_CTL_CURSOR_SET_IMAGE	_IOW('G', 12, struct ps3gpu_ctl_cursor_set_image)
#define	PS3GPU_CTL_CURSOR_SET_POSITION	_IOW('G', 13, struct ps3gpu_ctl_cursor_set_position)
#define	PS3GPU_CTL_CURSOR_ENABLE	_IOW('G', 14, struct ps3gpu_ctl_cursor_enable)

#endif /* _PS3GPU_CTL_H */
