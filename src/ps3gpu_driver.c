/*-
 * Copyright (C) 2011 glevand <geoffrey.levand@mail.ru>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer,
 *    without modification, immediately at the beginning of the file.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $FreeBSD$
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "xf86.h"
#include "xf86_OSproc.h"

#include "xf86Cursor.h"

#include "exa.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/uio.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>

#include "ps3gpu_ctl.h"

#include "ps3gpu.h"
#include "ps3gpu_cursor.h"
#include "ps3gpu_accel.h"

#define PS3GPU_VERSION		4000
#define PS3GPU_NAME		"ps3gpu"
#define PS3GPU_DRIVER_NAME	"ps3gpu"
#define PS3GPU_MAJOR_VERSION	0
#define PS3GPU_MINOR_VERSION	1
#define PS3GPU_PATCHLEVEL	0

#define PS3GPU_DEVICE_PATH	"/dev/ps3gpu"

#ifdef XFree86LOADER

static pointer Ps3GpuSetup(pointer module, pointer opts,
    int *errmaj, int *errmin);
static const OptionInfoRec *Ps3GpuAvailableOptions(int chipid, int busid);
static void Ps3GpuIdentify(int flags);
static Bool Ps3GpuProbe(DriverPtr drv, int flags);
static Bool Ps3GpuPreInit(ScrnInfoPtr pScrn, int flags);
static Bool Ps3GpuScreenInit(int scrnIndex, ScreenPtr pScreen,
    int argc, char **argv);
static Bool Ps3GpuCloseScreen(int scrnIndex, ScreenPtr pScreen);
static void Ps3GpuFreeScreen(int scrnIndex, int flags);
static Bool Ps3GpuSaveScreen(ScreenPtr pScreen, int mode);
static ModeStatus Ps3GpuValidMode(int scrnIndex, DisplayModePtr mode,
    Bool verbose, int flags);
static Bool Ps3GpuSwitchMode(int scrnIndex, DisplayModePtr mode, int flags);
static Bool Ps3GpuSetMode(ScrnInfoPtr pScrn, DisplayModePtr mode);
static void Ps3GpuAdjustFrame(int scrnIndex, int x, int y, int flags);
static Bool Ps3GpuEnterVT(int scrnIndex, int flags);
static void Ps3GpuLeaveVT(int scrnIndex, int flags);
static void Ps3GpuSave(ScrnInfoPtr pScrn);
static void Ps3GpuRestore(ScrnInfoPtr pScrn);
static Bool Ps3GpuMapMem(ScrnInfoPtr pScrn);
static Bool Ps3GpuUnmapMem(ScrnInfoPtr pScrn);
static Bool Ps3GpuDriverFunc(ScrnInfoPtr pScrn, xorgDriverFuncOp op,
    pointer ptr);

#endif /* XFree86LOADER */

_X_EXPORT DriverRec PS3GPU = {
	PS3GPU_VERSION,
	PS3GPU_DRIVER_NAME,
	Ps3GpuIdentify,
	Ps3GpuProbe,
	Ps3GpuAvailableOptions,
	NULL,
	0,
	Ps3GpuDriverFunc
};

static SymTabRec Ps3GpuChipsets[] = {
	{ 0, "ps3gpu" },
	{ -1, NULL }
};

enum {
	OPTION_SHADOW_FB,
	OPTION_SW_CURSOR,
	OPTION_NOACCEL
};

static const OptionInfoRec Ps3GpuOptions[] = {
	{ OPTION_SHADOW_FB, "ShadowFB", OPTV_BOOLEAN, { 0 }, FALSE },
	{ OPTION_SW_CURSOR, "SWcursor", OPTV_BOOLEAN, { 0 }, FALSE },
	{ OPTION_NOACCEL, "NoAccel", OPTV_BOOLEAN, { 0 }, FALSE },
	{ -1, NULL, OPTV_NONE, { 0 }, FALSE }
};

#ifdef XFree86LOADER

static XF86ModuleVersionInfo Ps3GpuVersRec = {
	"ps3gpu",
	MODULEVENDORSTRING,
	MODINFOSTRING1,
	MODINFOSTRING2,
	XORG_VERSION_CURRENT,
	PS3GPU_MAJOR_VERSION, 
	PS3GPU_MINOR_VERSION, 
	PS3GPU_PATCHLEVEL,
	ABI_CLASS_VIDEODRV,
	ABI_VIDEODRV_VERSION,
	NULL,
	{ 0, 0, 0, 0 }
};

_X_EXPORT XF86ModuleData ps3gpuModuleData = { &Ps3GpuVersRec, Ps3GpuSetup, NULL };

static pointer
Ps3GpuSetup(pointer module, pointer opts,
    int *errmaj, int *errmin)
{
	static Bool setupDone = FALSE;

	if (!setupDone) {
		setupDone = TRUE;
		xf86AddDriver(&PS3GPU, module, HaveDriverFuncs);
		return ((pointer) 1);
	} else {
		if (errmaj != NULL)
			*errmaj = LDR_ONCEONLY;
		return (NULL);
	}
}

#endif /* XFree86LOADER */

static Bool
Ps3GpuGetRec(ScrnInfoPtr pScrn)
{
	if (pScrn->driverPrivate)
		return (TRUE);

	pScrn->driverPrivate = xnfcalloc(sizeof(Ps3GpuRec), 1);

	return (TRUE);
}

static void
Ps3GpuFreeRec(ScrnInfoPtr pScrn)
{
	if (!pScrn->driverPrivate)
		return;

	xfree(pScrn->driverPrivate);

	pScrn->driverPrivate = NULL;
}

static const OptionInfoRec *
Ps3GpuAvailableOptions(int chipid, int busid)
{
	return (Ps3GpuOptions);
}

static void
Ps3GpuIdentify(int flags)
{
	xf86PrintChipsets(PS3GPU_NAME, "driver for PS3 GPU", Ps3GpuChipsets);
}

static Bool
Ps3GpuProbe(DriverPtr drv, int flags)
{
	ScrnInfoPtr pScrn = NULL;
	GDevPtr *devSections;
	int numDevSections;
	Bool foundScreen = FALSE;
	int i;

	if (flags & PROBE_DETECT)
		return (FALSE);

	numDevSections = xf86MatchDevice(PS3GPU_DRIVER_NAME, &devSections);
	if (numDevSections <= 0)
		return (FALSE);

	if (numDevSections > 1) {
		xf86Msg(X_ERROR, "Ignoring additional device sections\n");
		numDevSections = 1;
	}

	for (i = 0; i < numDevSections; i++) {
		int entityIndex = xf86ClaimFbSlot(drv, 0, devSections[i],
		    TRUE);

		pScrn = xf86ConfigFbEntity(NULL, 0, entityIndex,
		    NULL, NULL, NULL, NULL);
		if (pScrn) {
			pScrn->driverVersion = PS3GPU_VERSION;
			pScrn->driverName = PS3GPU_DRIVER_NAME;
			pScrn->name = PS3GPU_NAME;
			pScrn->Probe = Ps3GpuProbe;
			pScrn->PreInit = Ps3GpuPreInit;
			pScrn->ScreenInit = Ps3GpuScreenInit;
			pScrn->FreeScreen = Ps3GpuFreeScreen;
			pScrn->SwitchMode = Ps3GpuSwitchMode;
			pScrn->AdjustFrame = Ps3GpuAdjustFrame;
			pScrn->EnterVT = Ps3GpuEnterVT;
			pScrn->LeaveVT = Ps3GpuLeaveVT;
			pScrn->ValidMode = Ps3GpuValidMode;

			foundScreen = TRUE;
		}
	}

	xfree(devSections);

	return (foundScreen);	
}

static Bool
Ps3GpuPreInit(ScrnInfoPtr pScrn, int flags)
{
	Ps3GpuPtr gPtr;
	GDevPtr device = xf86GetEntityInfo(pScrn->entityList[0])->device;
	rgb rgbZeros = { 0, 0, 0 };
	rgb rgbMasks = { 0x00ff0000, 0x0000ff00, 0x000000ff };
	Gamma gammaZeroes = { 0.0, 0.0, 0.0 };
	DisplayModePtr mode;
	int i;

	if (flags & PROBE_DETECT)
		return (FALSE);

	pScrn->monitor = pScrn->confScreen->monitor;

	if (!Ps3GpuGetRec(pScrn))
		return (FALSE);

	gPtr = PS3GPUPTR(pScrn);

	pScrn->monitor = pScrn->confScreen->monitor;

	if (pScrn->numEntities > 1)
		return (FALSE);

	gPtr->pEnt = xf86GetEntityInfo(pScrn->entityList[0]);

	pScrn->chipset = (char *) xf86TokenToString(Ps3GpuChipsets,
	    gPtr->pEnt->chipset);

	xf86DrvMsg(pScrn->scrnIndex, X_INFO, "Chipset is a %s\n",
	    pScrn->chipset);

	if (!xf86SetDepthBpp(pScrn, 0, 0, 0,  Support32bppFb)) {
		return (FALSE);
	} else {
		switch (pScrn->depth) {
		case 24:
		break;
		default:
	    		xf86DrvMsg(pScrn->scrnIndex, X_ERROR,
			    "Given depth (%d) is not supported by this driver\n",
			    pScrn->depth);
	    		return (FALSE);
		}
	}

	xf86PrintDepthBpp(pScrn);

    	if (pScrn->depth == 8)
		pScrn->rgbBits = 8;

	if (!xf86SetWeight(pScrn, rgbZeros, rgbMasks))
		return (FALSE);

	if (!xf86SetDefaultVisual(pScrn, -1)) 
		return (FALSE);

	if (!xf86SetGamma(pScrn, gammaZeroes))
		return (FALSE);

	xf86CollectOptions(pScrn, device->options);

	if (!(gPtr->options = xalloc(sizeof(Ps3GpuOptions))))
		return (FALSE);

	memcpy(gPtr->options, Ps3GpuOptions, sizeof(Ps3GpuOptions));

	xf86ProcessOptions(pScrn->scrnIndex, pScrn->options, gPtr->options);

	if (device->videoRam != 0) {
		pScrn->videoRam = device->videoRam;
		xf86DrvMsg(pScrn->scrnIndex, X_CONFIG, "VideoRAM: %d kByte\n",
		    pScrn->videoRam);
	} else {
		pScrn->videoRam = 64 * 1024;
		xf86DrvMsg(pScrn->scrnIndex, X_PROBED, "VideoRAM: %d kByte\n",
		    pScrn->videoRam);
	}

	xf86GetOptValBool(gPtr->options, OPTION_SHADOW_FB, &gPtr->shadowFB);
	xf86GetOptValBool(gPtr->options, OPTION_SW_CURSOR, &gPtr->swCursor);
	xf86GetOptValBool(gPtr->options, OPTION_NOACCEL, &gPtr->noAccel);

	if (gPtr->shadowFB) {
		xf86DrvMsg(pScrn->scrnIndex, X_CONFIG,
		    "Using Shadow Framebuffer\n");
	}

	if (gPtr->swCursor) {
		xf86DrvMsg(pScrn->scrnIndex, X_CONFIG,
		    "Using Software Cursor\n");
	}

	if (!gPtr->noAccel) {
		xf86DrvMsg(pScrn->scrnIndex, X_CONFIG,
		    "Acceleration disabled\n");
	}

	pScrn->progClock = TRUE;

	mode = (DisplayModePtr) xalloc(sizeof(DisplayModeRec));
	mode->prev = mode;
	mode->next = mode;
	mode->name = "ps3gpu current mode";
	mode->status = MODE_OK;
	mode->type = M_T_BUILTIN;
	mode->Clock = 0;
	mode->HDisplay = 1920;
	mode->HSyncStart = 0;
	mode->HSyncEnd = 0;
	mode->HTotal = 0;
	mode->HSkew = 0;
	mode->VDisplay = 1080;
	mode->VSyncStart = 0;
	mode->VSyncEnd = 0;
	mode->VTotal = 0;
	mode->VScan = 0;
	mode->Flags = 0;

	if (pScrn->modes != NULL) {
		xf86DrvMsg(pScrn->scrnIndex, X_INFO,
		   "Ignoring mode specification from screen section\n");
	}

	pScrn->currentMode = pScrn->modes = mode;

	pScrn->virtualX = mode->HDisplay;
	pScrn->virtualY = mode->VDisplay;
	pScrn->displayWidth = pScrn->virtualX;

	xf86PrintModes(pScrn);

	xf86SetDpi(pScrn, 0, 0);

	if (gPtr->shadowFB) {
		if (!xf86LoadSubModule(pScrn, "shadow")) {
			Ps3GpuFreeRec(pScrn);
			return (FALSE);
		}
	}

	if (!xf86LoadSubModule(pScrn, "fb")) {
		Ps3GpuFreeRec(pScrn);
		return (FALSE);
	}

	if (!gPtr->noAccel) {
		if (!xf86LoadSubModule(pScrn, "exa")) {
			Ps3GpuFreeRec(pScrn);
			return (FALSE);
		}
	}

	return (TRUE);
}

static Bool
Ps3GpuScreenInit(int scrnIndex, ScreenPtr pScreen,
    int argc, char **argv)
{
	ScrnInfoPtr pScrn;
	Ps3GpuPtr gPtr;
	int err;
    
	pScrn = xf86Screens[pScreen->myNum];
	gPtr = PS3GPUPTR(pScrn);

	if (!Ps3GpuMapMem(pScrn))
		return (FALSE);

	if (!Ps3GpuSetMode(pScrn, pScrn->currentMode))
		return (FALSE);

	Ps3GpuAdjustFrame(scrnIndex, pScrn->frameX0, pScrn->frameY0, 0);

	miClearVisualTypes();

	err = miSetVisualTypes(pScrn->depth,
	    miGetDefaultVisualMask(pScrn->depth),
	    pScrn->rgbBits, pScrn->defaultVisual);
	if (!err)
		return (FALSE);

	if (!miSetPixmapDepths())
		return (FALSE);

	err = fbScreenInit(pScreen, gPtr->fbBase, pScrn->virtualX,
	    pScrn->virtualY, pScrn->xDpi, pScrn->yDpi, pScrn->displayWidth,
	    pScrn->bitsPerPixel);
	if (!err)
		return (FALSE);

	if (pScrn->depth > 8) {
		VisualPtr visual = pScreen->visuals + pScreen->numVisuals;

		while (--visual >= pScreen->visuals) {
			if ((visual->class | DynamicClass) == DirectColor) {
				visual->offsetRed = pScrn->offset.red;
				visual->offsetGreen = pScrn->offset.green;
				visual->offsetBlue = pScrn->offset.blue;
				visual->redMask = pScrn->mask.red;
				visual->greenMask = pScrn->mask.green;
				visual->blueMask = pScrn->mask.blue;
			}
		}
	}

	fbPictureInit(pScreen, 0, 0);

	xf86SetBlackWhitePixels(pScreen);

	miInitializeBackingStore(pScreen);
	xf86SetBackingStore(pScreen);
	xf86SetSilkenMouse(pScreen);
	
	miDCInitialize(pScreen, xf86GetPointerScreenFuncs());

	if (!gPtr->swCursor)
		Ps3GpuCursorInit(pScreen);

	if (!gPtr->noAccel)
		Ps3GpuAccelInit(pScreen);

	if(!miCreateDefColormap(pScreen))
		return (FALSE);

	pScreen->SaveScreen = Ps3GpuSaveScreen;

	gPtr->closeScreen = pScreen->CloseScreen;
   	pScreen->CloseScreen = Ps3GpuCloseScreen;

	if (serverGeneration == 1)
		xf86ShowUnusedOptions(pScrn->scrnIndex, pScrn->options);

	return (TRUE);
}

static Bool
Ps3GpuCloseScreen(int scrnIndex, ScreenPtr pScreen)
{
	ScrnInfoPtr pScrn = xf86Screens[scrnIndex];
	Ps3GpuPtr gPtr = PS3GPUPTR(pScrn);

	if (pScrn->vtSema) {
		Ps3GpuRestore(pScrn);
		Ps3GpuUnmapMem(pScrn);
	}

	if (gPtr->cursorInfo)
		xf86DestroyCursorInfoRec(gPtr->cursorInfo);

	pScrn->vtSema = FALSE;

	pScreen->CloseScreen = gPtr->closeScreen;

	return (*pScreen->CloseScreen)(scrnIndex, pScreen);
}

static void
Ps3GpuFreeScreen(int scrnIndex, int flags)
{
	Ps3GpuFreeRec(xf86Screens[scrnIndex]);
}

static Bool
Ps3GpuSaveScreen(ScreenPtr pScreen, int mode)
{
	return (TRUE);
}

static ModeStatus
Ps3GpuValidMode(int scrnIndex, DisplayModePtr mode,
    Bool verbose, int flags)
{
	return (MODE_OK);
}

static Bool
Ps3GpuSwitchMode(int scrnIndex, DisplayModePtr mode, int flags)
{
	return (TRUE);
}

static Bool
Ps3GpuSetMode(ScrnInfoPtr pScrn, DisplayModePtr mode)
{
	Ps3GpuPtr gPtr = PS3GPUPTR(pScrn);
	struct ps3gpu_ctl_flip flip;
	int err;

	pScrn->vtSema = TRUE;

	flip.context_id = gPtr->gpuContextId;
	flip.head = PS3GPU_CTL_HEAD_A;
	flip.offset = gPtr->fbHandle;

	err = ioctl(gPtr->fd, PS3GPU_CTL_FLIP, &flip);
	if (err) {
	    	xf86DrvMsg(pScrn->scrnIndex, X_ERROR,
		    "Failed to flip head A\n");
		return (FALSE);
	}

	flip.context_id = gPtr->gpuContextId;
	flip.head = PS3GPU_CTL_HEAD_B;
	flip.offset = gPtr->fbHandle;

	err = ioctl(gPtr->fd, PS3GPU_CTL_FLIP, &flip);
	if (err) {
	    	xf86DrvMsg(pScrn->scrnIndex, X_ERROR,
		    "Failed to flip head B\n");
		return (FALSE);
	}

	return (TRUE);
}

static void 
Ps3GpuAdjustFrame(int scrnIndex, int x, int y, int flags)
{
}

static Bool
Ps3GpuEnterVT(int scrnIndex, int flags)
{
	ScrnInfoPtr pScrn = xf86Screens[scrnIndex];

	if (!Ps3GpuSetMode(pScrn, pScrn->currentMode))
		return (FALSE);

	return (TRUE);
}

static void
Ps3GpuLeaveVT(int scrnIndex, int flags)
{
}

static void
Ps3GpuSave(ScrnInfoPtr pScrn)
{
}

static void
Ps3GpuRestore(ScrnInfoPtr pScrn)
{
}

static Bool
Ps3GpuMapMem(ScrnInfoPtr pScrn)
{
	Ps3GpuPtr gPtr = PS3GPUPTR(pScrn);
	struct ps3gpu_ctl_context_allocate contextAllocate;
	struct ps3gpu_ctl_memory_allocate memoryAllocate;
	int err;

	gPtr->fd = open(PS3GPU_DEVICE_PATH, O_RDWR | O_NONBLOCK);
	if (gPtr->fd < 0) {
	    	xf86DrvMsg(pScrn->scrnIndex, X_ERROR,
		    "Failed to open GPU device\n");
		return (FALSE);
	}

	gPtr->fbSize = (pScrn->videoRam - 1024) * 1024;

	contextAllocate.vram_size = pScrn->videoRam / 1024;

	err = ioctl(gPtr->fd, PS3GPU_CTL_CONTEXT_ALLOCATE,
	    &contextAllocate);
	if (err) {
	    	xf86DrvMsg(pScrn->scrnIndex, X_ERROR,
		    "Failed to allocate GPU context\n");
		return (FALSE);
	}

	gPtr->gpuContextId = contextAllocate.context_id;
	gPtr->gpuControlHandle = contextAllocate.control_handle;
	gPtr->gpuControlSize = contextAllocate.control_size;

	memoryAllocate.context_id = contextAllocate.context_id;
	memoryAllocate.type = PS3GPU_CTL_MEMORY_TYPE_VIDEO;
	memoryAllocate.size = gPtr->fbSize;
	memoryAllocate.align = 12;

	err = ioctl(gPtr->fd, PS3GPU_CTL_MEMORY_ALLOCATE,
	    &memoryAllocate);
	if (err) {
	    	xf86DrvMsg(pScrn->scrnIndex, X_ERROR,
		    "Failed to allocate framebuffer memory\n");
		return (FALSE);
	}

	gPtr->fbHandle = memoryAllocate.handle;
	gPtr->fbGpuAddress = memoryAllocate.gpu_addr;

	gPtr->fbBase = (pointer) mmap(NULL, gPtr->fbSize,
	    PROT_READ | PROT_WRITE, MAP_SHARED, gPtr->fd,
	    gPtr->fbHandle);
	if (gPtr->fbBase == (pointer *) MAP_FAILED) {
	    	xf86DrvMsg(pScrn->scrnIndex, X_ERROR,
		    "Failed to map framebuffer memory\n");
		return (FALSE);
	}

	return (TRUE);
}

static Bool
Ps3GpuUnmapMem(ScrnInfoPtr pScrn)
{
	Ps3GpuPtr gPtr = PS3GPUPTR(pScrn);

	close(gPtr->fd);

	gPtr->fbBase = NULL;

	return (TRUE);
}

static Bool
Ps3GpuDriverFunc(ScrnInfoPtr pScrn, xorgDriverFuncOp op,
    pointer ptr)
{
	CARD32 *flag;
    
	switch (op) {
	case GET_REQUIRED_HW_INTERFACES:
		flag = (CARD32 *) ptr;
		*flag = 0;
		return (TRUE);
	default:
		return (FALSE);
    	}
}
